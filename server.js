const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("../d1/routes/taskRoute.js");

const app = express();
const port = 3001;
// MongoDB connection
mongoose.connect(
  "mongodb+srv://admin:admin@batch245-celestino.ldu2t4p.mongodb.net/s35-discussion?retryWrites=true&w=majority",
  {
    //Allows us to avoid any current and future errors while connecting to MONGODB
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

// error catcher
db.on("error", console.error.bind(console, "Connection Error!"));
// confirmation of the connection
db.once("open", () => console.log("We are now connected to the cloud!"));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/tasks", taskRoute);

app.listen(port, () => {
  console.log(`Server is running at port ${port}!`);
});
