const express = require("express");
const { append } = require("vary");
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

// Routes

router.get("/", taskControllers.getAll);
//Route for createTask
router.post("/addTask", taskControllers.createTask);
//Router for deleteTask
router.delete("/deleteTask/:id", taskControllers.deleteTask);
//Router for specificTask
router.get("/:id", taskControllers.specificTask);
//Router for updateTask
router.put("/:id/complete", taskControllers.updateTask);
module.exports = router;

//
